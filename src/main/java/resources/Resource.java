package resources;

import beans.User;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("users")
public class Resource {

    private static final Map<String, User> users = new HashMap<>();

    static {
        users.put("1", new User(1, "Dima"));
        users.put("2", new User(2, "Katya"));
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response getUserById(final @PathParam("id") String id){
        final User user = users.get(id);
        final Gson json = new Gson();
        return Response.ok(json.toJson(user)).build();
    }


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUsers(){
        final Gson json = new Gson();
        return Response.ok(json.toJson(json.toJson(users))).build();
    }

}
