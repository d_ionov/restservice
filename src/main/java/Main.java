import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

public class Main {

    public static final String BASE_URL = "http://localhost:8080/";

    public static HttpServer startServer(){
        final ResourceConfig config = new ResourceConfig().packages("resources");
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URL), config);
    }

    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Start server.."));
        System.in.read();
        server.stop();
    }


}
